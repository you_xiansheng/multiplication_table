﻿using System;
using System.Threading;

namespace 九九乘法表
{
    class Program
    {
        static void Main(string[] args)
        {

            for (int i = 1; i <= 9; i++)
            {
                for (int j = 1; j <= i; j++)
                {
                    Console.WriteLine(i + "*" + j + "=" + i * j + "\t");
                }
                Console.WriteLine();
            }

        }
    }
}
